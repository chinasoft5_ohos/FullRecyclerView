## FullRecyclerView


#### 项目介绍
- 项目名称：FullRecyclerView
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现recyclerView中不同种类和操作的效果。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支


#### 效果演示
|[Swipe DragAndDrop](image/Draglist.gif) | [Swipe](image/Swipelist.gif) | [Grid](image/Grid.gif) | [Horizontal](image/Horizontallist.gif)|
| :---:        |     :---:      |     :---:       |     :---:      |
|![transition_dragAndDrop](image/Draglist.gif) | ![transition_swipe](image/Swipelist.gif)| ![animation_grid](image/Grid.gif)| ![animation_horizontal](image/Horizontallist.gif)| 
|[Add Item](image/Snap.gif) | [Delete Item](image/vertical)| [Expandable](image/expandable) | [Animation](image/animation)|
|![animation_addItem](image/Verticallist.gif) | ![animation_deleteItem](image/Verticallist.gif)| ![transition_expandable](image/Expandable.gif)| ![animation](image/Animation.gif)|
|[Multiple Choice](image/multiple) | [Single Choice](image/single) | [Update Data](image/updateData) | [Snap](image/snap)|
|![animation_multiple](image/Multiplechoice.gif)| ![animation_single](image/Singlechoice.gif)| ![animation update data](image/Updatedata.gif)| ![animation_snap_vertical](image/Snap.gif)|
|[Section](image/sections) | [Add Favorites](image/addfavorites) | [Section with Lines](image/sectionwithline) | [Indexed](image/indexed)|
|![animation_section](image/Sections.gif)| ![animation_addfavorites](image/AddFavorites.gif)| ![animation_sectionwihtlines](image/Sectionwithline.gif)| ![animation_indexed](image/Indexed.gif)|


#### 安装教程
 该组件是纯app，不包含library，没有上传到maven仓库  
 1.下载app的hap包FullRecyclerView.hap(位于 https://gitee.com/chinasoft5_ohos/FullRecyclerView/releases/1.0.0 )
 
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 使用说明

具体使用方法请下载demo体验

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息

```
    Copyright 2017 Javier González Cabezas

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
```
