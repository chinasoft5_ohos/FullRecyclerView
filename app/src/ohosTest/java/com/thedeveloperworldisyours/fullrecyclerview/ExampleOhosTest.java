package com.thedeveloperworldisyours.fullrecyclerview;

import com.thedeveloperworldisyours.fullrecycleview.updateData.UpdateData;
import com.thedeveloperworldisyours.fullrecycleview.updateData.UpdateDataFraction;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    @Test
    public void testDatas() {
        ArrayList<UpdateData> dataSet = UpdateDataFraction.getDataSet();
        assertEquals(20, dataSet.size());
    }

    @Test
    public void testDatasEquals() {
        ArrayList<UpdateData> dataSet = UpdateDataFraction.getDataSet();
        assertNotEquals(19, dataSet.size());
    }

    @Test
    public void testDatasNull() {
        ArrayList<UpdateData> dataSet = UpdateDataFraction.getDataSet();
        assertNotNull(dataSet);
    }

}