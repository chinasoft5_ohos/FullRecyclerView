package com.thedeveloperworldisyours.fullrecycleview.animation;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by javierg on 25/01/2017.
 */

public class AnimationRecyclerViewAdapter extends BaseItemProvider {

    private ArrayList<AnimationData> mDataset;
    private HashSet<Integer> positionSet = new HashSet<>();
    private LayoutScatter layoutScatter;

    static class DataObjectHolder {
        Text mLabel;
        Text mDateTime;

        DataObjectHolder(Component itemView) {
            mLabel = (Text) itemView.findComponentById(ResourceTable.Id_vertical_list_item_title);
            mDateTime = (Text) itemView.findComponentById(ResourceTable.Id_vertical_list_item_subtitle);
        }
    }

    AnimationRecyclerViewAdapter(LayoutScatter scatter, ArrayList<AnimationData> myDataset) {
        this.mDataset = myDataset;
        layoutScatter = scatter;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder = null;
        if (component == null) {
            component = layoutScatter.parse(ResourceTable.Layout_vertical_list_item, componentContainer, false);

            dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }
        dataObjectHolder.mLabel.setText(mDataset.get(position).getmTitle());
        dataObjectHolder.mDateTime.setText(mDataset.get(position).getmSubTitle());
        // Here you apply the animation when the view is bound
        setAnimation(component, position);
        return component;
    }

    void addItem(AnimationData dataObj, int index) {
        mDataset.add(dataObj);
        notifyDataSetItemChanged(index);
    }

    void deleteItem(int index) {
        mDataset.remove(index);
        notifyDataSetItemRemoved(index);
    }

    /**
     * Here is the key method to apply the animation
     *
     * @param viewToAnimate 组件
     * @param position      位置
     */
    private void setAnimation(Component viewToAnimate, int position) {

        viewToAnimate.setBindStateChangedListener(new Component.BindStateChangedListener() {
            private AnimatorValue animatorValue;

            @Override
            public void onComponentBoundToWindow(Component component) {
                if (positionSet.contains(position)) {
                    component.setAlpha(1f);
                    component.setTranslationX(0);
                    return;
                }
                animatorValue = new AnimatorValue();
                animatorValue.setDuration(400);
                animatorValue.setValueUpdateListener((animatorValue, v) -> {
                    component.setAlpha(v);
                    component.setTranslationX(-component.getWidth() / 2.0f + (v * component.getWidth() / 2.0f));
                });
                animatorValue.start();
                positionSet.add(position);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                if (animatorValue != null) {
                    animatorValue.cancel();
                    animatorValue.release();
                }
                component.setAlpha(0);
                component.setTranslationX(-component.getWidth() / 2.0f);
            }
        });
    }

}
