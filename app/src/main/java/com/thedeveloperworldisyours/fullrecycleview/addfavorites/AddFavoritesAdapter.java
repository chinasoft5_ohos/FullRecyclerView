package com.thedeveloperworldisyours.fullrecycleview.addfavorites;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.addfavorites.model.ElementList;
import com.thedeveloperworldisyours.fullrecycleview.addfavorites.model.Fruit;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import ohos.app.Context;

import java.util.List;

/**
 * Created by javierg on 03/08/2017.
 */

public class AddFavoritesAdapter extends BaseItemProvider {

    private static final int SECTION = 0;
    private static final int NORMAL = 1;
    private List<ElementList> mList;
    private MyClickListener sClickListener;
    private LayoutScatter layoutScatter;

    AddFavoritesAdapter(LayoutScatter scatter, List<ElementList> list) {
        this.mList = list;
        layoutScatter = scatter;
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        sClickListener = myClickListener;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        int componentType = getItemComponentType(position);
        if (componentType == NORMAL) {
            component = layoutScatter.parse(ResourceTable.Layout_add_favorites_list_item, componentContainer, false);
            DataObjectHolder dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
            Fruit fruit = (Fruit) mList.get(position);
            dataObjectHolder.mName.setText(fruit.getName());
            dataObjectHolder.mName.setFont(Font.DEFAULT_BOLD);
            dataObjectHolder.mFavorite.setPixelMap(fruit.isFavourite() ? ResourceTable.Media_btn_star_big_on
                    : ResourceTable.Media_btn_star_big_off);
            dataObjectHolder.mFavorite.setClickedListener((Component sComponent) -> {
                if (fruit.isFavourite()) {
                    sClickListener.onItemClick(position, false);
                } else {
                    sClickListener.onItemClick(position, true);
                }
            });
        } else {
            component = layoutScatter.parse(ResourceTable.Layout_add_favorites_section_list_item, componentContainer, false);
            SectionHolder sectionHolder = new SectionHolder(component);
            component.setTag(sectionHolder);
            sectionHolder.mTextViewSection.setText(mList.get(position).getName());
        }
        return component;
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }


    @Override
    public long getItemId(int i) {
        return Integer.toUnsignedLong(i);
    }

    public void refreshData(List<ElementList> dataset) {
        mList.clear();
        mList.addAll(dataset);
        notifyDataChanged();
    }

    interface MyClickListener {
        void onItemClick(int position, boolean addItem);
    }

    @Override
    public int getItemComponentType(int position) {
        if (mList.get(position).isSection()) {
            return SECTION;
        } else {
            return NORMAL;
        }
    }


    static class SectionHolder {
        Text mTextViewSection;

        SectionHolder(Component itemView) {
            mTextViewSection = (Text) itemView.findComponentById(ResourceTable.Id_add_favorites_list_item_text);
        }
    }


    static class DataObjectHolder {
        Text mName;
        Image mFavorite;

        DataObjectHolder(Component itemView) {
            mName = (Text) itemView.findComponentById(ResourceTable.Id_add_favorites_item_name);
            mFavorite = (Image) itemView.findComponentById(ResourceTable.Id_add_favorites_item_favorite);
        }
    }


}
