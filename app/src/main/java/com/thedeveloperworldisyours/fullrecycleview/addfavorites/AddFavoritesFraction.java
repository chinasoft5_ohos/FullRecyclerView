package com.thedeveloperworldisyours.fullrecycleview.addfavorites;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.addfavorites.model.ElementList;
import com.thedeveloperworldisyours.fullrecycleview.addfavorites.model.Fruit;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class AddFavoritesFraction extends Fraction implements AddFavoritesAdapter.MyClickListener {

    private ArrayList<ElementList> mList;
    private ListContainer mListContainer;
    private AddFavoritesAdapter mAdapter;
    private ArrayList<ElementList> mFavoritesAndHeadersList;
    private ArrayList<Fruit> mRestList;
    private Context context;

    public AddFavoritesFraction() {
        // Required empty public constructor
    }

    public static AddFavoritesFraction newInstance() {
        return new AddFavoritesFraction();
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        context = container.getContext();
        Component component = scatter.parse(ResourceTable.Layout_add_favorites_fragment, container, false);
        mListContainer = (ListContainer) component.findComponentById(ResourceTable.Id_add_favorites_fragment_recycler_view);

        mList = getDataSet();
        mAdapter = new AddFavoritesAdapter(scatter, mList);
        mAdapter.setOnItemClickListener(this);
        mListContainer.setItemProvider(mAdapter);

        mRestList = new ArrayList<>();
        mFavoritesAndHeadersList = new ArrayList<>();

        return component;
    }

    private ArrayList<ElementList> getDataSet() {
        Fruit elementList;
        ArrayList results = new ArrayList<>();
        String[] fruits = {
                "Apples", "Apricots", "Avocado", "Annona", "Banana",
                "Bilberry", "Blackberry", "Custard", "Clementine", "Cantalope",
                "Coconut", "Currant", "Cherry", "Cherimoya", "Date",
                "Damson", "Durian", "Elderberry", "Fig", "Feijoa",
                "Grapefruit", "Grape", "Gooseberry", "Guava", "Honeydew",
                "Huckleberry", "Jackfruit", "Juniper", "Jambul", "Jujube",
                "Kiwi", "Kumquat", "Lemons", "Limes", "Lychee",
                "Mango", "Mandarin", "Mangostine", "Nectaraine", "Orange",
                "Olive", "Prunes", "Pears", "Plum", "Pineapple",
                "Peach", "Papaya", "Passionfruit", "Pomegranate", "Pomelo",
                "Raspberries", "Rock melon", "Rambutan", "Strawberries", "Sweety",
                "Salmonberry", "Satsuma", "Tangerines", "Tomato", "Ugli",
                "Watermelon", "Woodapple",
        };

        for (int i = 0; i < fruits.length; i++) {
            elementList = new Fruit(fruits[i], false, i, false);
            results.add(i, elementList);
        }
        return results;
    }

    @Override
    public void onItemClick(int position, boolean addItem) {
        if (addItem) {
            mAdapter.refreshData(addFavorite(mList, position));
        } else {
            mAdapter.refreshData(deleteFavorite(mList, position));
        }
    }

    public List<ElementList> addFavorite(List<ElementList> list, int position) {

        List<ElementList> result = new ArrayList<>();
        Fruit fruit = (Fruit) list.get(position);
        fruit.setFavourite(true);


        result.add(0, new ElementList(context.getString(ResourceTable.String_add_favorites_title), true));
        result.add(1, list.get(position));


        if (list.get(0).isSection()) {

            mFavoritesAndHeadersList.clear();
            // filled Favorite List
            mFavoritesAndHeadersList.add(0, new ElementList(context.getString(ResourceTable.String_add_favorites_title), true));
            for (int i = 1; i < list.size(); i++) {
                if (i == 1) {
                    mFavoritesAndHeadersList.add(i, fruit);
                    mFavoritesAndHeadersList.add(i + 1, list.get(i));
                } else {
                    mFavoritesAndHeadersList.add(i + 1, list.get(i));
                    if (list.get(i).isSection()) {
                        break;
                    }
                }
            }

            // filled result List
            for (int i = 1; i < list.size(); i++) {
                if (position > i) {
                    result.add(i + 1, list.get(i));
                } else if (position < i) {
                    result.add(i, list.get(i));
                }
            }

            // removed item from rest list
            mRestList.remove(fruit);

        } else {
            // first favorite
            mFavoritesAndHeadersList.add(0, new ElementList(context.getString(ResourceTable.String_add_favorites_title), true));
            mFavoritesAndHeadersList.add(1, list.get(position));
            mFavoritesAndHeadersList.add(2, new ElementList(context.getString(ResourceTable.String_add_favorites_no_favorites), true));

            result.add(2, new ElementList(context.getString(ResourceTable.String_add_favorites_no_favorites), true));

            for (int i = 0; i < list.size(); i++) {
                if (position > i) {
                    result.add(i + 3, list.get(i));
                    mRestList.add(i, (Fruit) list.get(i));
                } else if (position < i) {
                    result.add(i + 2, list.get(i));
                    mRestList.add(i - 1, (Fruit) list.get(i));
                }
            }
        }


        return result;
    }

    public List<ElementList> deleteFavorite(List<ElementList> list, int position) {

        List<ElementList> result = new ArrayList<>();
        ArrayList<Fruit> newRestList = new ArrayList<>();
        Fruit fruit = (Fruit) list.get(position);
        fruit.setFavourite(false);
        mFavoritesAndHeadersList.remove(fruit);
        boolean addedFruit = false;

        if (countFavorite(list) > 0) {
            // Added favorites and headers
            for (int i = 0; i < mFavoritesAndHeadersList.size(); i++) {
                result.add(mFavoritesAndHeadersList.get(i));
            }

            if (mRestList.size() == 0 || !mRestList.contains(fruit)) {
                mRestList.add(fruit);
                addedFruit = true;
            }

            // Added rest of elements
            for (int i = 0; i < mRestList.size(); i++) {
                if (fruit.getIndex() > mRestList.get(i).getIndex()) {
                    result.add(mRestList.get(i));
                    newRestList.add(mRestList.get(i));
                } else {
                    if (!addedFruit) {

                        result.add(fruit);
                        newRestList.add(fruit);
                        addedFruit = true;
                    }
                    result.add(mRestList.get(i));
                    newRestList.add(mRestList.get(i));
                }

            }

            // Update restList
            mRestList.clear();
            mRestList = newRestList;

        } else {
            // there is not favorite remove both header
            result = getDataSet();
            mFavoritesAndHeadersList.clear();
            mRestList.clear();
        }
        return result;
    }

    private int countFavorite(List<ElementList> list) {
        int number = 0;
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).isSection()) {
                if (((Fruit) list.get(i)).isFavourite()) {
                    number++;
                }
            }
        }
        return number;
    }

}
