package com.thedeveloperworldisyours.fullrecycleview.dragandswipe;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.adapter.DragAndSwipeRecyclerListAdapter;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by javierg on 12/10/2016.
 */

public class DragAndSwipeListFraction extends Fraction implements ListContainer.ItemLongClickedListener, Component.TouchEventListener {


    private ListContainer listContainer;
    private DragAndSwipeRecyclerListAdapter adapter;

    public DragAndSwipeListFraction() {
    }

    public static DragAndSwipeListFraction newInstance() {
        DragAndSwipeListFraction fragment = new DragAndSwipeListFraction();
        return fragment;
    }

    private int downPosition = -1;

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component view = scatter.parse(ResourceTable.Layout_vertical_fragment, container, false);
        listContainer = (ListContainer) view.findComponentById(ResourceTable.Id_vertical_fragment_recycler_view);
        adapter = new DragAndSwipeRecyclerListAdapter(scatter, null);
        listContainer.setOrientation(Component.VERTICAL);
        listContainer.setItemProvider(adapter);
        listContainer.setItemLongClickedListener(this);
        listContainer.setTouchEventListener(this);
        return view;
    }


    private int findTouchPosition(int downPosition, float upY) {
        //拿到当前位置的position
        for (int i = 0; i < listContainer.getVisibleIndexCount(); i++) {
            int position = listContainer.getItemPosByVisibleIndex(i);
            if (position == downPosition) {
                continue;
            }
            int top = listContainer.getComponentAt(position).getTop();
            int bottom = listContainer.getComponentAt(position).getBottom();
            int hight = bottom - top;
            if (upY >= (double) top && (upY <= (double) (top + hight / 2))) {
                return position == 0 ? position : (position - 1);
            } else if ((upY > (top + hight / 2) && upY < bottom)) {
                return position > downPosition ? position : position + 1;
            } else if (upY < 0) {
                return 0;
            } else if (upY > listContainer.getComponentAt(listContainer.getItemPosByVisibleIndex(listContainer.getVisibleIndexCount() - 1)).getBottom()) {
                return listContainer.getItemPosByVisibleIndex(listContainer.getVisibleIndexCount() - 1);
            }
        }
        return -1;
    }

    @Override
    public boolean onItemLongClicked(ListContainer listContainer, Component component, int i, long l) {
        downPosition = i;
        return false;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
                if (downPosition != -1) {
                    int touchPosition = findTouchPosition(downPosition, touchEvent.getPointerPosition(touchEvent.getIndex()).getY());
                    if (touchPosition != -1) {
                        String remove = adapter.getmItems().remove(downPosition);
                        adapter.notifyDataSetItemRemoved(downPosition);
                        adapter.getmItems().add(touchPosition, remove);
                        adapter.notifyDataSetItemInserted(touchPosition);
                    }
                }
                downPosition = -1;
                break;
        }
        return true;
    }
}
