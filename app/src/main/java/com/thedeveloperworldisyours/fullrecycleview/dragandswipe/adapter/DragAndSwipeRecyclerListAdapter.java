package com.thedeveloperworldisyours.fullrecycleview.dragandswipe.adapter;

import com.demo.swipe.RippleUtil;
import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.helper.ItemTouchHelperAdapter;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.helper.ItemTouchHelperViewHolder;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.helper.OnStartDragListener;
import ohos.agp.components.*;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by javierg on 12/10/2016.
 */

public class DragAndSwipeRecyclerListAdapter extends BaseItemProvider
        implements ItemTouchHelperAdapter {

    private LayoutScatter layoutScatter;
    public List<String> getmItems() {
        return mItems;
    }

    private List<String> mItems = new ArrayList<>();

    private final OnStartDragListener mDragStartListener;

    public DragAndSwipeRecyclerListAdapter(LayoutScatter scatter, OnStartDragListener dragStartListener) {
        layoutScatter = scatter;
        mDragStartListener = dragStartListener;
        mItems.clear();
        mItems.add("One");
        mItems.add("Two");
        mItems.add("Three");
        mItems.add("Four");
        mItems.add("Five");
        mItems.add("Six");
        mItems.add("Seven");
        mItems.add("Eight");
        mItems.add("Nine");
        mItems.add("Ten");
    }

    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        notifyDataSetItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mItems, fromPosition, toPosition);
        notifyDataSetItemRangeChanged(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }


    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ItemViewHolder itemViewHolder = null;
        if (component == null) {
            component = layoutScatter.parse(ResourceTable.Layout_drag_swipe_list_item, componentContainer, false);

            itemViewHolder = new ItemViewHolder(component);
            component.setTag(itemViewHolder);
            RippleUtil.setComponent(component);
        } else {
            itemViewHolder = (ItemViewHolder) component.getTag();
        }
        itemViewHolder.textView.setText(mItems.get(position));
        // Start a drag whenever the handle view it touched
        itemViewHolder.handleView.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
//                        mDragStartListener.onStartDrag(itemViewHolder);
                }
                return false;
            }
        });
        return component;
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder implements
            ItemTouchHelperViewHolder {

        public final Text textView;
        public final Image handleView;

        public ItemViewHolder(Component itemView) {
            textView = (Text) itemView.findComponentById(ResourceTable.Id_drag_swipe_list_item_text);
            handleView = (Image) itemView.findComponentById(ResourceTable.Id_drag_swipe_list_item_handle);
        }

        @Override
        public void onItemSelected() {
//            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
//            itemView.setBackgroundColor(0);
        }
    }
}
