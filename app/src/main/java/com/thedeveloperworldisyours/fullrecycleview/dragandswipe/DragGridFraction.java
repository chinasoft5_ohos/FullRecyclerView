package com.thedeveloperworldisyours.fullrecycleview.dragandswipe;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.adapter.DragAndDropGridAdapter;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.helper.OnStartDragListener;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.DisplayManager;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by javierg on 12/10/2016.
 */

public class DragGridFraction extends Fraction implements OnStartDragListener, Component.TouchEventListener, ListContainer.ItemLongClickedListener {
    private static final int GRID_COLUMNS = 2;
    private DragAndDropGridAdapter adapter;
    private ListContainer listContainer;

    public DragGridFraction() {
    }

    public static DragGridFraction newInstance() {
        DragGridFraction fragment = new DragGridFraction();
        return fragment;
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component view = scatter.parse(ResourceTable.Layout_vertical_fragment, container, false);
        listContainer = (ListContainer) view.findComponentById(ResourceTable.Id_vertical_fragment_recycler_view);

        int width = DisplayManager.getInstance().getDefaultDisplay(container.getContext()).get().getAttributes().width / GRID_COLUMNS;
        adapter = new DragAndDropGridAdapter(scatter, this, width);
        listContainer.setItemProvider(adapter);

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(GRID_COLUMNS);
        // tableLayoutManager.setOrientation(Component.HORIZONTAL);
        listContainer.setLayoutManager(tableLayoutManager);

        listContainer.setTouchEventListener(this);
        listContainer.setItemLongClickedListener(this);
        return view;
    }

    @Override
    public void onStartDrag(Component viewHolder) {
    }

    private int downPosition = -1;

    private int findTouchPosition(int downPosition, float upY, float upX) {
        //拿到当前位置的position
        for (int i = 0; i < listContainer.getVisibleIndexCount(); i++) {
            int position = listContainer.getItemPosByVisibleIndex(i);
            if (position == downPosition) {
                continue;
            }
            int top = listContainer.getComponentAt(position).getTop();
            int bottom = listContainer.getComponentAt(position).getBottom();
            int left = listContainer.getComponentAt(position).getLeft();
            int right = listContainer.getComponentAt(position).getRight();

            if (upX > left && upX < right && upY > top && upY < bottom) {
                return position;
            } else if (upY < 0) {
                return 0;
            } else if (upY > listContainer.getComponentAt(listContainer.getItemPosByVisibleIndex(listContainer.getVisibleIndexCount() - 1)).getBottom()) {
                return listContainer.getItemPosByVisibleIndex(listContainer.getVisibleIndexCount() - 1);
            }
        }
        return -1;
    }

    @Override
    public boolean onItemLongClicked(ListContainer listContainer, Component component, int i, long l) {
        downPosition = i;
        return false;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
                if (downPosition != -1) {
                    int touchPosition = findTouchPosition(downPosition, touchEvent.getPointerPosition(touchEvent.getIndex()).getY(), touchEvent.getPointerPosition(touchEvent.getIndex()).getX());
                    if (touchPosition != -1) {
                        String remove = adapter.getmItems().remove(downPosition);
                        adapter.notifyDataSetItemRemoved(downPosition);
                        adapter.getmItems().add(touchPosition, remove);
                        adapter.notifyDataSetItemInserted(touchPosition);
                    }
                }
                downPosition = -1;
                break;
            case TouchEvent.CANCEL:
                downPosition = -1;
                break;
        }
        return true;
    }
}
