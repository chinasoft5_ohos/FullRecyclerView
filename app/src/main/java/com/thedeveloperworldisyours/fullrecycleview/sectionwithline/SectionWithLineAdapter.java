package com.thedeveloperworldisyours.fullrecycleview.sectionwithline;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * Created by javierg on 08/08/2017.
 */

public class SectionWithLineAdapter extends BaseItemProvider {

    public static final int ITEM_FINAL = 0;
    public static final int ITEM = 1;
    public static final int SECTION = 2;
    private List<ElementList> mList;
    private LayoutScatter scatter;

    static class SectionHolder {
        Text mTextViewSection;

        SectionHolder(Component itemView) {
            mTextViewSection = (Text) itemView.findComponentById(ResourceTable.Id_section_with_line_list_item_header_text);
        }
    }

    static class DataObjectHolder {
        Text mName;
        Component mView;

        DataObjectHolder(Component itemView) {
            mName = (Text) itemView.findComponentById(ResourceTable.Id_section_with_line_list_item_name);
            mView = (Component) itemView.findComponentById(ResourceTable.Id_section_with_line_list_item_view);
        }
    }

    public SectionWithLineAdapter(LayoutScatter scatter, List<ElementList> list) {
        this.scatter = scatter;
        mList = list;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        int type = getItemComponentType(position);
        ElementList elementList = mList.get(position);
        if (component == null) {
            if (type == ITEM) {
                component = scatter.parse(ResourceTable.Layout_section_with_line_list_item, componentContainer, false);
                DataObjectHolder dataObjectHolder = new DataObjectHolder(component);
                component.setTag(dataObjectHolder);
                dataObjectHolder.mName.setText(elementList.getName());
            } else if (type == ITEM_FINAL) {
                component = scatter.parse(ResourceTable.Layout_section_with_line_list_item, componentContainer, false);
                DataObjectHolder dataObjectHolderFinal = new DataObjectHolder(component);
                component.setTag(dataObjectHolderFinal);
                dataObjectHolderFinal.mName.setText(elementList.getName());
                dataObjectHolderFinal.mView.setVisibility(Component.HIDE);
            } else {
                component = scatter.parse(ResourceTable.Layout_section_with_lien_list_item_header, componentContainer, false);
                SectionHolder sectionHolder = new SectionHolder(component);
                component.setTag(sectionHolder);
                sectionHolder.mTextViewSection.setText(elementList.getName());
            }
        } else {
            if (component.getTag() instanceof DataObjectHolder) {
                if (ITEM == getItemComponentType(position)) {
                    ((DataObjectHolder) component.getTag()).mName.setText(elementList.getName());
                } else {
                    if (type == ITEM_FINAL) {
                        ((DataObjectHolder) component.getTag()).mName.setText(elementList.getName());
                        ((DataObjectHolder) component.getTag()).mView.setVisibility(Component.HIDE);
                    }
                }
            } else {
                if (component.getTag() instanceof SectionHolder) {
                    ((SectionHolder) component.getTag()).mTextViewSection.setText(elementList.getName());
                }
            }
        }

        return component;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    public void refreshData(List<ElementList> dataset) {
        mList.clear();
        mList.addAll(dataset);
        notifyDataChanged();
    }

    @Override
    public int getItemComponentType(int position) {
        if (mList.get(position).isSection()) {
            return SECTION;
        } else {
            if (mList.get(position).ismNextSection()) {
                return ITEM_FINAL;
            } else {
                return ITEM;
            }
        }
    }
}
