package com.thedeveloperworldisyours.fullrecycleview.sectionwithline;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.*;

public class SectionWithLineFraction extends Fraction {

    HashMap<String, Integer> mMapIndex;
    String[] mSections;
    List<String> fruits;
    ListContainer mListContainer;

    SectionWithLineAdapter mAdapter;

    public SectionWithLineFraction() {
        // Required empty public constructor
    }

    public static SectionWithLineFraction newInstance() {
        return new SectionWithLineFraction();
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_section_with_line_fragment, container, false);
        mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_section_with_line_fragment_recycler_view);
        mListContainer.setScrollbarFadingEnabled(true);
        mAdapter = new SectionWithLineAdapter(scatter, getDataSet());
        mListContainer.setItemProvider(mAdapter);
        return view;
    }


    private ArrayList<ElementList> getDataSet() {
        String[] fruits = {
                "Apples", "Apricots", "Avocado", "Annona", "Banana",
                "Bilberry", "Blackberry", "Custard", "Clementine", "Cantalope",
                "Coconut", "Currant", "Cherry", "Cherimoya", "Date",
                "Damson", "Durian", "Elderberry", "Fig", "Feijoa",
                "Grapefruit", "Grape", "Gooseberry", "Guava", "Honeydew",
                "Huckleberry", "Jackfruit", "Juniper", "Jambul", "Jujube",
                "Kiwi", "Kumquat", "Lemons", "Limes", "Lychee",
                "Mango", "Mandarin", "Mangostine", "Nectaraine", "Orange",
                "Olive", "Prunes", "Pears", "Plum", "Pineapple",
                "Peach", "Papaya", "Passionfruit", "Pomegranate", "Pomelo",
                "Raspberries", "Rock melon", "Rambutan", "Strawberries", "Sweety",
                "Salmonberry", "Satsuma", "Tangerines", "Tomato", "Ugli",
                "Watermelon", "Woodapple",
        };

        List<String> fruitList = Arrays.asList(fruits);
        getListIndexed(fruitList);
        ArrayList results = new ArrayList<>();
        ElementList obj;
        int section = 0;
        int normal = 0;
        String fruit;
        String ch;
        int total = fruitList.size() + mSections.length;
        for (int index = 0; index < total; index++) {
            fruit = fruitList.get(normal);
            ch = fruit.substring(0, 1);
            if (index == 0 || ch.equals(mSections[section])) {
                if (index != 0) {
                    obj = new ElementList(fruitList.get(normal - 1), false, true);
                    results.add(index - 1, obj);
                }
                obj = new ElementList(ch, true, false);
                mMapIndex.put(ch, index);
                if (section < mSections.length - 1) {
                    section++;
                } else {
                    section = 0;
                }
            } else {
                obj = new ElementList(fruitList.get(normal), false, false);
                normal++;
            }
            results.add(index, obj);
        }
        return results;
    }


    public void getListIndexed(List<String> fruitList) {

        this.fruits = fruitList;
        mMapIndex = new LinkedHashMap<>();

        for (int x = 0; x < fruits.size(); x++) {
            String fruit = fruits.get(x);
            String ch = fruit.substring(0, 1);
            ch = ch.toUpperCase(Locale.US);

            // HashMap will prevent duplicates
            mMapIndex.put(ch, x);
        }

        Set<String> sectionLetters = mMapIndex.keySet();

        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<>(sectionLetters);
        Collections.sort(sectionList);
        mSections = new String[sectionList.size()];
        sectionList.toArray(mSections);
    }

}
