package com.thedeveloperworldisyours.fullrecycleview.snap;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

public class SnapFraction extends Fraction {

    public SnapFraction() {
        // Required empty public constructor
    }

    public static SnapFraction newInstance() {
        return new SnapFraction();
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_snap_fragment, container, false);

        ListContainer listContainerVertical = (ListContainer) view.findComponentById(ResourceTable.Id_snap_fragment_vertical_recycler_view);
        ListContainer listContainer = (ListContainer) view.findComponentById(ResourceTable.Id_snap_fragment_horizontal_recycler_view);

        SnapData dataThe = new SnapData("The", ResourceTable.Media_thedeveloperworldisyours);
        SnapData dataDeveloper = new SnapData("Developer", ResourceTable.Media_thewordis);
        SnapData dataWorld = new SnapData("World", ResourceTable.Media_theworldisyours);
        SnapData dataIs = new SnapData("Is", ResourceTable.Media_thedeveloperworldisyours);
        SnapData dataYours = new SnapData("Yours", ResourceTable.Media_thewordis);
        SnapData dataCom = new SnapData(".com", ResourceTable.Media_theworldisyours);

        ArrayList<SnapData> list = new ArrayList<>();

        list.add(0, dataThe);
        list.add(1, dataDeveloper);
        list.add(2, dataWorld);
        list.add(3, dataIs);
        list.add(4, dataYours);
        list.add(5, dataCom);

        SnapHorizontalRecyclerViewAdapter adapter = new SnapHorizontalRecyclerViewAdapter(scatter, list);
        listContainer.setItemProvider(adapter);
        listContainer.setOrientation(Component.HORIZONTAL);
        listContainerVertical.setItemProvider(adapter);

        return view;
    }

}
