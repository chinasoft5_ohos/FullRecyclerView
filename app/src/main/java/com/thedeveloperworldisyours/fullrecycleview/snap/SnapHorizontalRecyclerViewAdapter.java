package com.thedeveloperworldisyours.fullrecycleview.snap;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by javierg on 06/02/2017.
 */

public class SnapHorizontalRecyclerViewAdapter extends BaseItemProvider {

    private ArrayList<SnapData> mDataSet;
    private LayoutScatter scatter;

    public SnapHorizontalRecyclerViewAdapter(LayoutScatter scatter,ArrayList<SnapData> dataset) {
        this.scatter = scatter;
        this.mDataSet = dataset;
    }

    @Override
    public int getCount() {
        return mDataSet.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSet.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder;
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_snap_list_item, componentContainer, false);
            dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }
        dataObjectHolder.mTextView.setText(mDataSet.get(position).getText());
        dataObjectHolder.mImage.setPixelMap(mDataSet.get(position).getImage());
        return component;
    }

    public static class DataObjectHolder implements Component.ClickedListener {

        Image mImage;
        Text mTextView;

        public DataObjectHolder(Component itemView) {
            mImage = (Image) itemView.findComponentById(ResourceTable.Id_snap_list_item_image_view);
            mTextView = (Text) itemView.findComponentById(ResourceTable.Id_snap_list_item_text_view);
            itemView.setClickedListener(this);
        }

        @Override
        public void onClick(Component view) {

        }
    }

}
