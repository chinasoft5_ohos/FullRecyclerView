package com.thedeveloperworldisyours.fullrecycleview.chat;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * Created by javierg on 25/10/2017.
 */

public class ChatFraction extends Fraction {

    ListContainer mListContainer;

    public ChatFraction() {
        // Required empty public constructor
    }

    public static ChatFraction newInstance() {
        return new ChatFraction();
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component component = scatter.parse(ResourceTable.Layout_chat_fragment, container, false);
        mListContainer = (ListContainer) component.findComponentById(ResourceTable.Id_chat_recycler_view);
        ChatAdapter adapter = new ChatAdapter(scatter, getDataSet());
        mListContainer.setItemProvider(adapter);
        return component;
    }

    private ArrayList<ChatData> getDataSet() {
        ArrayList results = new ArrayList<>();
        ChatData obj;
        obj = new ChatData(0, "23 December ", "");
        results.add(0, obj);
        for (int index = 1; index < 20; index++) {
            if ((index & 1) == 0) {
                obj = new ChatData(2, "Hello Text " + index,
                        "22:01 ");
            } else {
                obj = new ChatData(1, "Hello Text " + index,
                        "22:01 ");
            }
            results.add(index, obj);
        }
        return results;
    }
}
