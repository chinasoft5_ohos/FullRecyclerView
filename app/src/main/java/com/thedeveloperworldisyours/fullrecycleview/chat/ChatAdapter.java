package com.thedeveloperworldisyours.fullrecycleview.chat;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by javierg on 25/10/2017.
 */

public class ChatAdapter extends BaseItemProvider {

    private static final int TIME = 0;
    private static final int RECEIVE_MESSAGE = 1;
    private static final int SEND_MESSAGE = 2;

    private ArrayList<ChatData> mDataset;
    private LayoutScatter layoutScatter;

    static class ReceiveObjectHolder {
        Text mReceiverText;

        ReceiveObjectHolder(Component itemView) {
            mReceiverText = (Text) itemView.findComponentById(ResourceTable.Id_receive_text_textView);
        }
    }


    static class SendObjectHolder {
        Text mSendText;

        SendObjectHolder(Component itemView) {
            mSendText = (Text) itemView.findComponentById(ResourceTable.Id_send_text_textView);
        }
    }


    static class TimeHolder {
        Text mTimeTextView;

        TimeHolder(Component itemView) {
            mTimeTextView = (Text) itemView.findComponentById(ResourceTable.Id_chat_time);
        }
    }

    public ChatAdapter(LayoutScatter scatter,ArrayList<ChatData> dataset) {
        mDataset = dataset;
        layoutScatter = scatter;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ChatData chatData = mDataset.get(position);
        if (component == null) {
            int componentType = getItemComponentType(position);
            if (componentType == RECEIVE_MESSAGE) {
                component = layoutScatter.parse(ResourceTable.Layout_chat_item_list_receive_message, componentContainer, false);
                ReceiveObjectHolder receiveObjectHolder = new ReceiveObjectHolder(component);
                component.setTag(receiveObjectHolder);
                receiveObjectHolder.mReceiverText.setText(chatData.getTitle());
            } else if (componentType == SEND_MESSAGE) {
                component = layoutScatter.parse(ResourceTable.Layout_chat_item_list_send_message, componentContainer, false);
                SendObjectHolder sendObjectHolder = new SendObjectHolder(component);
                component.setTag(sendObjectHolder);
                sendObjectHolder.mSendText.setText(chatData.getTitle());
            } else {
                component = layoutScatter.parse(ResourceTable.Layout_chat_item_list_time, componentContainer, false);
                TimeHolder timeHolder = new TimeHolder(component);
                component.setTag(timeHolder);
                timeHolder.mTimeTextView.setText(chatData.getTitle());
            }
        } else {
            if (component.getTag() instanceof ReceiveObjectHolder) {
                ((ReceiveObjectHolder) component.getTag()).mReceiverText.setText(chatData.getTitle());
            } else if (component.getTag() instanceof SendObjectHolder) {
                ((SendObjectHolder) component.getTag()).mSendText.setText(chatData.getTitle());
            } else {
                if (component.getTag() instanceof TimeHolder) {
                    ((TimeHolder) component.getTag()).mTimeTextView.setText(chatData.getTitle());
                }
            }
        }
        return component;
    }


    @Override
    public int getItemComponentType(int position) {
        switch (mDataset.get(position).getElement()) {
            case 0:
                return TIME;
            case 1:
                return RECEIVE_MESSAGE;
            case 2:
                return SEND_MESSAGE;
            default:
                return TIME;
        }
    }

}
