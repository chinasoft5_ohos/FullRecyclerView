package com.thedeveloperworldisyours.fullrecycleview.multiple;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * Created by javierg on 01/02/2017.
 */

public class MultipleRecyclerViewAdapter extends BaseItemProvider {

    private List<MultipleData> mList;
    private LayoutScatter scatter;

    MultipleRecyclerViewAdapter(LayoutScatter scatter, List<MultipleData> mList) {
        this.scatter = scatter;
        this.mList = mList;
    }

    static class DataObjectHolder {

        Text mTextView;
        RadioButton mRadioButton;

        DataObjectHolder(Component itemView) {
            mTextView = (Text) itemView.findComponentById(ResourceTable.Id_multiple_list_item_text);
            mRadioButton = (RadioButton) itemView.findComponentById(ResourceTable.Id_multiple_list_item_check_button);
            mRadioButton.setButtonElement(null);
        }
    }

    void changedData(int position) {
        if (mList.get(position).isBoolean()) {
            mList.get(position).setBoolean(false);
        } else {
            mList.get(position).setBoolean(true);
        }
        notifyDataChanged();
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder = null;
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_multiple_list_item, componentContainer, false);

            dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }
        if (dataObjectHolder != null) {
            dataObjectHolder.mTextView.setText(mList.get(position).getTitle());
            dataObjectHolder.mRadioButton.setChecked(mList.get(position).isBoolean());
        }
        return component;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getCount() {
        return mList.size();
    }


}
