package com.thedeveloperworldisyours.fullrecycleview.multipleclicks;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by javierg on 16/11/2017.
 */

public class MultipleClicksAdapter extends BaseItemProvider {

    private ArrayList<MultipleClickData> mDataset;
    private MultipleClickListener sClickListener;
    private LayoutScatter scatter;

    class DataObjectHolder {
        Text mOne;
        Text mTwo;
        Text mThree;

        DataObjectHolder(Component itemView, int position) {
            mOne = (Text) itemView.findComponentById(ResourceTable.Id_multiple_clicks_one_textView);
            mTwo = (Text) itemView.findComponentById(ResourceTable.Id_multiple_clicks_two_textView);
            mThree = (Text) itemView.findComponentById(ResourceTable.Id_multiple_clicks_three_textView);

            mOne.setClickedListener((Component view) ->
                    sClickListener.onItemClickOne(position, mOne));

            mOne.setLongClickedListener((Component view) ->
                    sClickListener.onItemClickOne(position, mOne));

            mTwo.setClickedListener((Component view) ->
                    sClickListener.onItemClickTwo(position, mTwo));

            mTwo.setLongClickedListener((Component view) ->
                    sClickListener.onItemClickTwo(position, mTwo));

            mThree.setClickedListener((Component view) ->
                    sClickListener.onItemClickThree(position, mThree));

            mThree.setLongClickedListener((Component view) ->
                    sClickListener.onItemClickThree(position, mThree));
        }

    }

    void setOnItemClickListener(MultipleClickListener myClickListener) {
        this.sClickListener = myClickListener;
    }

    MultipleClicksAdapter(LayoutScatter scatter, ArrayList<MultipleClickData> myDataset) {
        this.scatter = scatter;
        mDataset = myDataset;
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder;
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_multiple_clicks_list_item, componentContainer, false);
            dataObjectHolder = new DataObjectHolder(component, position);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }
        if (dataObjectHolder != null) {
            dataObjectHolder.mOne.setText(mDataset.get(position).getmTitle());
            dataObjectHolder.mTwo.setText(mDataset.get(position).getmSubTitle());
            dataObjectHolder.mThree.setText(mDataset.get(position).getmThirdTitle());
        }
        return component;
    }

    interface MultipleClickListener {
        void onItemClickOne(int position, Component view);

        void onItemClickTwo(int position, Component view);

        void onItemClickThree(int position, Component view);
    }

}
