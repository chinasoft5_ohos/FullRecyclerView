package com.thedeveloperworldisyours.fullrecycleview.multipleclicks;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;

/**
 * Created by javierg on 16/11/2017.
 */

public class MultipleClicksFraction extends Fraction implements MultipleClicksAdapter.MultipleClickListener {

    MultipleClicksAdapter mAdapter;

    ListContainer mListContainer;
    Context context;

    public MultipleClicksFraction() {
        // Required empty public constructor
    }

    public static MultipleClicksFraction newInstance() {
        return new MultipleClicksFraction();
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_multiple_clicks_fragment, container, false);
        context = container.getContext();
        mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_multiple_clicks_fragment_recycler_view);
        mAdapter = new MultipleClicksAdapter(scatter, getData());
        mListContainer.setItemProvider(mAdapter);
        mAdapter.setOnItemClickListener(this);
        return view;
    }


    public ArrayList<MultipleClickData> getData() {

        ArrayList<MultipleClickData> list = new ArrayList<>();
        MultipleClickData one = new MultipleClickData("One", "Two", "Three");
        MultipleClickData two = new MultipleClickData("One", "Two", "Three");
        MultipleClickData three = new MultipleClickData("One", "Two", "Three");

        MultipleClickData one1 = new MultipleClickData("One", "Two", "Three");
        MultipleClickData two1 = new MultipleClickData("One", "Two", "Three");
        MultipleClickData three1 = new MultipleClickData("One", "Two", "Three");

        MultipleClickData one2 = new MultipleClickData("One", "Two", "Three");
        MultipleClickData two2 = new MultipleClickData("One", "Two", "Three");
        MultipleClickData three2 = new MultipleClickData("One", "Two", "Three");

        MultipleClickData one3 = new MultipleClickData("One", "Two", "Three");
        MultipleClickData two3 = new MultipleClickData("One", "Two", "Three");
        MultipleClickData three3 = new MultipleClickData("One", "Two", "Three");

        list.add(one);
        list.add(two);
        list.add(three);

        list.add(one1);
        list.add(two1);
        list.add(three1);

        list.add(one2);
        list.add(two2);
        list.add(three2);

        list.add(one3);
        list.add(two3);
        list.add(three3);

        return list;

    }

    @Override
    public void onItemClickOne(int position, Component v) {
        showDialog(0);
    }

    @Override
    public void onItemClickTwo(int position, Component v) {
        showDialog(1);
    }

    @Override
    public void onItemClickThree(int position, Component v) {
        showDialog(2);
    }

    public void showDialog(int type) {

        CommonDialog dialog = new CommonDialog(context);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_multiple_clicks_dialog, null, false);
        dialog.setCornerRadius(5);
        dialog.setTransparent(true);
        dialog.setContentCustomComponent(component);
        dialog.setSize(700, ComponentContainer.LayoutConfig.MATCH_CONTENT);

        Image imageView = (Image) component.findComponentById(ResourceTable.Id_multiple_clicks_close_imageView);
        Text oneText = (Text) component.findComponentById(ResourceTable.Id_multiple_clicks_one_textView);
        Text twoText = (Text) component.findComponentById(ResourceTable.Id_multiple_clicks_two_textView);
        Text threeText = (Text) component.findComponentById(ResourceTable.Id_multiple_clicks_three_textView);
        imageView.setPixelMap(ResourceTable.Media_btn_dialog_normal);
        imageView.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        imageView.setPixelMap(ResourceTable.Media_btn_dialog_pressed);
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        imageView.setPixelMap(ResourceTable.Media_btn_dialog_normal);
                        break;
                }
                return false;
            }
        });


        switch (type) {
            case 0:
                oneText.setTextColor(Color.BLACK);
                oneText.setSelected(true);
                break;
            case 1:
                twoText.setTextColor(Color.BLACK);
                twoText.setSelected(true);
                break;
            case 2:
                threeText.setTextColor(Color.BLACK);
                threeText.setSelected(true);
                break;
        }

        imageView.setClickedListener((Component view) -> dialog.hide());
        dialog.setSwipeToDismiss(true);
        dialog.show();

    }

}
