package com.thedeveloperworldisyours.fullrecycleview;

import com.thedeveloperworldisyours.fullrecycleview.slice.MainAbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class MainAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#cfcfcf"));
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
