/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thedeveloperworldisyours.fullrecycleview.common;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * 自定义 Dialog
 */

public class CustomDialog extends CommonDialog {
    private Component view;

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param layout  布局id
     */

    public CustomDialog(Context context, Component layout) {
        super(context);
        this.view = layout;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        setContentCustomComponent(view);
        setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    }
}
