package com.thedeveloperworldisyours.fullrecycleview.slice;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.addfavorites.AddFavoritesFraction;
import com.thedeveloperworldisyours.fullrecycleview.animation.AnimationFraction;
import com.thedeveloperworldisyours.fullrecycleview.chat.ChatFraction;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.DragAndSwipeListFraction;
import com.thedeveloperworldisyours.fullrecycleview.dragandswipe.DragGridFraction;
import com.thedeveloperworldisyours.fullrecycleview.expandable.ExpandableFraction;
import com.thedeveloperworldisyours.fullrecycleview.horizontal.HorizontalFraction;
import com.thedeveloperworldisyours.fullrecycleview.indexed.IndexedFraction;
import com.thedeveloperworldisyours.fullrecycleview.multiple.MultipleFraction;
import com.thedeveloperworldisyours.fullrecycleview.multipleclicks.MultipleClicksFraction;
import com.thedeveloperworldisyours.fullrecycleview.sections.SectionFraction;
import com.thedeveloperworldisyours.fullrecycleview.sectionwithline.SectionWithLineFraction;
import com.thedeveloperworldisyours.fullrecycleview.single.SingleFraction;
import com.thedeveloperworldisyours.fullrecycleview.snap.SnapFraction;
import com.thedeveloperworldisyours.fullrecycleview.stickyheader.StickyHeaderFraction;
import com.thedeveloperworldisyours.fullrecycleview.swipe.SwipeListFraction;
import com.thedeveloperworldisyours.fullrecycleview.updateData.UpdateDataFraction;
import com.thedeveloperworldisyours.fullrecycleview.vertical.VerticalFraction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.bundle.AbilityInfo;

import java.util.ArrayList;

public class MainAbilitySlice extends AbilitySlice implements ListContainer.ItemClickedListener,
        Component.ClickedListener {

    private static final int MULTIPLE = 0;
    private static final int SINGLE = 1;

    private PopupDialog popupDialog;
    private Image meunImage;
    private ArrayList<String> meunDatas = new ArrayList<>();

    private Fraction mFraction;
    private VerticalFraction mVerticalFraction;
    private UpdateDataFraction mUpdateDataFraction;

    private int mMode = 0;
    private Image addItem;
    private Text textMode;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initComponents();
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                setFraction(currentIndex);
                addFraction();
                if (popupDialog != null && popupDialog.isShowing()) {
                    popupDialog.hide();
                    initMeun();
                }
            }
        });
    }

    private void initComponents() {
        meunImage = (Image) findComponentById(ResourceTable.Id_image_meun);
        addItem = (Image) findComponentById(ResourceTable.Id_image_add);
        textMode = (Text) findComponentById(ResourceTable.Id_text_mode);

        meunImage.setClickedListener(this);
        addItem.setClickedListener(this);
        textMode.setClickedListener(this);

        addItem.setVisibility(Component.HIDE);
        textMode.setVisibility(Component.HIDE);

        getDatas();
        mFraction = DragAndSwipeListFraction.newInstance();
        addFraction();
    }

    private void getDatas() {
        meunDatas.add("Drag list");
        meunDatas.add("Grid");
        meunDatas.add("Swipe list");
        meunDatas.add("Horizontal list");
        meunDatas.add("Vertical list");
        meunDatas.add("Expandable");
        meunDatas.add("Multiple choice");
        meunDatas.add("Single choice");
        meunDatas.add("Snap");
        meunDatas.add("Animation");
        meunDatas.add("Sections");
        meunDatas.add("Indexed");
        meunDatas.add("Add Favorites");
        meunDatas.add("Section with line");
        meunDatas.add("Sticky header");
        meunDatas.add("Chat");
        meunDatas.add("Update data");
        meunDatas.add("Multiple clicks");
    }

    private int currentMenuPosition;
    private int currentIndex;

    private void initMeun() {
        getUITaskDispatcher().asyncDispatch(() -> {
            Component component = LayoutScatter.getInstance(MainAbilitySlice.this).parse(ResourceTable.Layout_popup_meun, null, false);
            ListContainer listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_popup_meun);
            MeunAdapter adapter = new MeunAdapter(MainAbilitySlice.this, meunDatas);
            listContainer.setItemProvider(adapter);
            listContainer.setItemClickedListener(MainAbilitySlice.this);
            listContainer.scrollTo(currentMenuPosition);
            listContainer.setScrollListener(new ListContainer.ScrollListener() {
                @Override
                public void onScrollFinished() {
                    currentMenuPosition = listContainer.getItemPosByVisibleIndex(0);
                }
            });
            popupDialog = new PopupDialog(MainAbilitySlice.this, component);
            popupDialog.setCustomComponent(component);
            popupDialog.setAutoClosable(true);
            popupDialog.setTransparent(true);
            popupDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
            popupDialog.setAlignment(LayoutAlignment.RIGHT);
            if (popupDialog != null && !popupDialog.isShowing()) {
                popupDialog.show();
            }
        });
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int index, long l) {
        setFraction(index);
        if (popupDialog != null && popupDialog.isShowing()) {
            popupDialog.hide();
        }
        addFraction();
        currentIndex = index;
    }

    private void setFraction(int index) {
        switch (meunDatas.get(index)) {
            case "Drag list":
                mFraction = DragAndSwipeListFraction.newInstance();
                break;
            case "Grid":
                mFraction = DragGridFraction.newInstance();
                break;
            case "Swipe list":
                mFraction = SwipeListFraction.newInstance();
                break;
            case "Horizontal list":
                mFraction = HorizontalFraction.newInstance();
                break;
            case "Vertical list":
                mFraction = VerticalFraction.newInstance();
                mVerticalFraction = (VerticalFraction) mFraction;
                break;
            case "Expandable":
                mFraction = ExpandableFraction.newInstance();
                break;
            case "Multiple choice":
                mFraction = MultipleFraction.newInstance();
                break;
            case "Single choice":
                mFraction = SingleFraction.newInstance();
                break;
            case "Snap":
                mFraction = SnapFraction.newInstance();
                break;
            case "Animation":
                mFraction = AnimationFraction.newInstance();
                break;
            case "Sections":
                mFraction = SectionFraction.newInstance();
                break;
            case "Indexed":
                mFraction = IndexedFraction.newInstance();
                break;
            case "Add Favorites":
                mFraction = AddFavoritesFraction.newInstance();
                break;
            case "Section with line":
                mFraction = SectionWithLineFraction.newInstance();
                break;
            case "Sticky header":
                mFraction = StickyHeaderFraction.newInstance();
                break;
            case "Chat":
                mFraction = ChatFraction.newInstance();
                break;
            case "Multiple clicks":
                mFraction = MultipleClicksFraction.newInstance();
                break;
            case "Update data":
                textMode.setText(mMode == MULTIPLE ? "CHANGE TO SINGLE" : "CHANGE TO MULTIPLE");
                mFraction = UpdateDataFraction.newInstance();
                mUpdateDataFraction = (UpdateDataFraction) mFraction;
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_image_meun:
                initMeun();
                break;
            case ResourceTable.Id_text_mode:
                updateMenuTitles();
                break;
            case ResourceTable.Id_image_add:
                mVerticalFraction.addItem();
                break;
        }
    }

    private void updateMenuTitles() {
        if (mMode == MULTIPLE) {
            mMode = SINGLE;
            textMode.setText("CHANGE TO MULTIPLE");
        } else {
            mMode = MULTIPLE;
            textMode.setText("CHANGE TO SINGLE");
        }
        if (mUpdateDataFraction != null) {
            mUpdateDataFraction.changeMode(mMode);
        }
    }

    public void addFraction() {
        getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                addItem.setVisibility(mFraction instanceof VerticalFraction ?
                        Component.VISIBLE : Component.HIDE);
                textMode.setVisibility(mFraction instanceof UpdateDataFraction ?
                        Component.VISIBLE : Component.HIDE);
                FractionScheduler fractionScheduler = ((FractionAbility) getAbility()).getFractionManager()
                        .startFractionScheduler();

                fractionScheduler.replace(ResourceTable.Id_content, mFraction)
                        .submit();
            }
        });
    }

}
