/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thedeveloperworldisyours.fullrecycleview.slice;

import com.demo.swipe.RippleUtil;
import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

public class MeunAdapter extends BaseItemProvider {

    private ArrayList<String> mDataset;
    private Context context;

    public MeunAdapter(Context context, ArrayList<String> mDataset) {
        this.context = context;
        this.mDataset = mDataset;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemComponentType(int position) {
        return super.getItemComponentType(position);
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Text text;
        if (component == null) {
            component = LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_meun_itme, componentContainer, false);
            text = (Text) component.findComponentById(ResourceTable.Id_texz);
            component.setTag(text);
            RippleUtil.setComponent(component);
        } else {
            text = (Text) component.getTag();
        }
        text.setText(mDataset.get(position));
        return component;
    }
}
