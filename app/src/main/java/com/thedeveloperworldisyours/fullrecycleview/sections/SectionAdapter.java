package com.thedeveloperworldisyours.fullrecycleview.sections;

import com.demo.swipe.RippleUtil;
import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.sections.model.ElementList;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;

import java.util.HashSet;
import java.util.List;

/**
 * Created by javierg on 14/07/2017.
 */

public class SectionAdapter extends BaseItemProvider {

    private static final int SECTION = 0;
    private static final int NORMAL = 1;
    private List<ElementList> mList;
    private LayoutScatter scatter;
    private HashSet<Integer> positionSet = new HashSet<>();

    static class SectionHolder {
        Text mTextViewSection;

        SectionHolder(Component itemView) {
            mTextViewSection = (Text) itemView.findComponentById(ResourceTable.Id_section_list_item_text);
        }
    }

    static class DataObjectHolder {
        Text mName;
        Image mImage;

        DataObjectHolder(Component itemView) {
            mName = (Text) itemView.findComponentById(ResourceTable.Id_section_item_name);
            mImage = (Image) itemView.findComponentById(ResourceTable.Id_section_item_favorite);
        }
    }

    public SectionAdapter(LayoutScatter scatter, List<ElementList> list) {
        this.scatter = scatter;
        mList = list;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ElementList elementList = mList.get(position);
        int type = getItemComponentType(position);
        if (component == null) {
            if (type == SECTION) {
                component = scatter.parse(ResourceTable.Layout_section_section_list_item, componentContainer, false);
                SectionHolder sectionHolder = new SectionHolder(component);
                component.setTag(sectionHolder);
                sectionHolder.mTextViewSection.setText(elementList.getName());
            } else {
                component = scatter.parse(ResourceTable.Layout_section_item_list, componentContainer, false);
                DataObjectHolder dataObjectHolder = new DataObjectHolder(component);
                dataObjectHolder.mName.setText(elementList.getName());
                dataObjectHolder.mImage.setPixelMap(elementList.isChecked() ? ResourceTable.Media_btn_star_big_on
                        : ResourceTable.Media_btn_star_big_off);
                component.setTag(dataObjectHolder);
                setAnimation(component, position);
                RippleUtil.setComponentClickListener(dataObjectHolder.mImage, new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        elementList.setChecked(!elementList.isChecked());
                        notifyDataSetItemChanged(position);
                    }
                });
            }
        } else {
            if (type == NORMAL) {
                ((DataObjectHolder) component.getTag()).mName.setText(mList.get(position).getName());
                ((DataObjectHolder) component.getTag()).mImage.setPixelMap(elementList.isChecked() ? ResourceTable.Media_btn_star_big_on
                        : ResourceTable.Media_btn_star_big_off);
                setAnimation(component, position);
                RippleUtil.setComponentClickListener(((DataObjectHolder) component.getTag()).mImage, new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        elementList.setChecked(!elementList.isChecked());
                        notifyDataSetItemChanged(position);
                    }
                });
            } else {
                ((SectionHolder) component.getTag()).mTextViewSection.setText(mList.get(position).getName());
            }
        }
        return component;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    /**
     * Here is the key method to apply the animation
     *
     * @param viewToAnimate 需要动画的控件
     * @param position      位置
     */
    private void setAnimation(Component viewToAnimate, int position) {

        viewToAnimate.setBindStateChangedListener(new Component.BindStateChangedListener() {
            private AnimatorValue animatorValue;

            @Override
            public void onComponentBoundToWindow(Component component) {
                if (positionSet.contains(position)) {
                    component.setAlpha(1f);
                    component.setTranslationX(0);
                    return;
                }
                animatorValue = new AnimatorValue();
                animatorValue.setDuration(400);
                animatorValue.setValueUpdateListener((animatorValue, v) -> {
                    component.setAlpha(v);
                    component.setTranslationX(-component.getWidth() / 2.0f + (v * component.getWidth() / 2.0f));
                });
                animatorValue.start();
                positionSet.add(position);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                if (animatorValue != null) {
                    animatorValue.cancel();
                    animatorValue.release();
                }
                component.setAlpha(0);
                component.setTranslationX(-component.getWidth() / 2.0f);
            }
        });
    }

    public void refreshData(List<ElementList> dataset) {
        mList.clear();
        mList.addAll(dataset);
        notifyDataChanged();
    }

    @Override
    public int getItemComponentType(int position) {
        if (mList.get(position).isSection()) {
            return SECTION;
        } else {
            return NORMAL;
        }
    }
}
