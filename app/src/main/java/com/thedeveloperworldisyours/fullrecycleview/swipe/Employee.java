package com.thedeveloperworldisyours.fullrecycleview.swipe;

import com.demo.swipe.SwipeLayout;

import java.io.Serializable;

/**
 * 实体类
 *
 * @author javierg
 * @version 1.0, 12/10/2016
 */

public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String emailId;

    private SwipeLayout.DragEdge dragEdge;

    public SwipeLayout.DragEdge getDragEdge() {
        return dragEdge;
    }

    public void setDragEdge(SwipeLayout.DragEdge dragEdge) {
        this.dragEdge = dragEdge;
    }

    public Employee() {

    }

    public Employee(String name, String emailId) {
        this.name = name;
        this.emailId = emailId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }


}
