package com.thedeveloperworldisyours.fullrecycleview.swipe;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.swipe.adapter.SwipeRecyclerViewAdapter;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * Created by javierg on 12/10/2016.
 */

public class SwipeListFraction extends Fraction {

    private ArrayList<Employee> mDataSet;
    private ListContainer mListContainer;
    private SwipeRecyclerViewAdapter adapter;

    public static SwipeListFraction newInstance() {
        SwipeListFraction fragment = new SwipeListFraction();

        return fragment;
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component view = scatter.parse(ResourceTable.Layout_swipe_fragment, container, false);
        mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_swipe_recycler_view);
        adapter = new SwipeRecyclerViewAdapter(container.getContext(), scatter, mDataSet);
        mListContainer.setItemProvider(adapter);

        mDataSet = new ArrayList<Employee>();

        loadData();

        if (mDataSet.isEmpty()) {
            mListContainer.setVisibility(Component.HIDE);
        } else {
            mListContainer.setVisibility(Component.VISIBLE);
        }

        SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(container.getContext(), scatter, mDataSet);

        mListContainer.setItemProvider(mAdapter);

        return view;
    }


    // load initial data
    public void loadData() {
        for (int i = 0; i <= 20; i++) {
            mDataSet.add(new Employee("Employee " + i, "ohosstudent" + i + "@gmail.com"));
        }
    }

}