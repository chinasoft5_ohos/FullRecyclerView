package com.thedeveloperworldisyours.fullrecycleview.expandable;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * Created by javierg on 16/01/2017.
 */

public class Artist implements Sequenceable {

    private String name;
    private boolean isFavorite;

    public Artist(String name, boolean isFavorite) {
        this.name = name;
        this.isFavorite = isFavorite;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        return parcel.writeBoolean(isFavorite) && parcel.writeString(name);
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.isFavorite = parcel.readBoolean();
        this.name = parcel.readString();
        return false;
    }

    public String getName() {
        return name;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public static final Sequenceable.Producer CREATOR = new Sequenceable.Producer() {
        public Artist createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            Artist instance = new Artist(in.readString(), in.readBoolean());
            instance.unmarshalling(in);
            return instance;
        }
    };


}
