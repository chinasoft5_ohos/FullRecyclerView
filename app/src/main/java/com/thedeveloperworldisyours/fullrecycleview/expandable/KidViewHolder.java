package com.thedeveloperworldisyours.fullrecycleview.expandable;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * Created by javierg on 16/01/2017.
 */

public class KidViewHolder {

    private Text childTextView;

    public KidViewHolder(Component itemView) {
        childTextView = (Text) itemView.findComponentById(ResourceTable.Id_list_item_artist_name);
    }

    public void setArtistName(String name) {
        childTextView.setText(name);
    }
}
