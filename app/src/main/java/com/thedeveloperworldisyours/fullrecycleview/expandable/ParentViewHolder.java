package com.thedeveloperworldisyours.fullrecycleview.expandable;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;

import java.util.List;

/**
 * Created by javierg on 16/01/2017.
 */

public class ParentViewHolder implements Component.ClickedListener, Animator.StateChangedListener {

    private Text genreName;
    private Image arrow;
    private Image icon;
    private ListContainer listContainer;
    private ListContainer plistContainer;
    private ExpandableKidsAdapter adapter;
    private boolean expand = false;
    private List<Genre> groups;
    private int position;
    private LayoutScatter scatter;

    public ParentViewHolder(LayoutScatter scatter, Component itemView, List<Genre> groups, int position, ListContainer slistContainer, ExpandListener expandListener) {
        this.scatter = scatter;
        this.plistContainer = slistContainer;
        this.position = position;
        this.groups = groups;
        this.expandListener = expandListener;
        genreName = (Text) itemView.findComponentById(ResourceTable.Id_list_item_genre_name);
        arrow = (Image) itemView.findComponentById(ResourceTable.Id_list_item_genre_arrow);
        icon = (Image) itemView.findComponentById(ResourceTable.Id_list_item_genre_icon);
        listContainer = (ListContainer) itemView.findComponentById(ResourceTable.Id_list_container);
        adapter = new ExpandableKidsAdapter(scatter, groups.get(position).getItems());
        listContainer.setItemProvider(adapter);
        arrow.setClickedListener(this);
        listContainer.setVisibility(groups.get(position).isExpand() ? Component.VISIBLE : Component.HIDE);
    }

    public void setGenreTitle(Genre genre) {
        genreName.setText(genre.getTitle());
        icon.setPixelMap(genre.getIconResId());
    }

    public void expand() {
        animateExpand();
    }

    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        AnimatorProperty property = arrow.createAnimatorProperty();
        property.rotate(180);
        property.setTarget(arrow);
        property.setDuration(300L);
        property.setStateChangedListener(this);
        property.start();
    }

    private void animateCollapse() {
        AnimatorProperty property = arrow.createAnimatorProperty();
        property.rotate(-180);
        property.setTarget(arrow);
        property.setDuration(300L);
        property.setStateChangedListener(this);
        property.start();
    }

    @Override
    public void onClick(Component component) {
        expand = !expand;
        if (expand) {
            expand();
        } else {
            collapse();
        }
    }

    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {
        groups.get(position).setExpand(!groups.get(position).isExpand());
        listContainer.setVisibility(groups.get(position).isExpand() ? Component.VISIBLE : Component.HIDE);
        if (expandListener != null) {
            expandListener.expand();
        }
    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

    /**
     * ExpandListener
     */
    public interface ExpandListener {
        /**
         * expand
         */
        void expand();
    }

    private ExpandListener expandListener;

}
