package com.thedeveloperworldisyours.fullrecycleview.expandable;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

public class ExpandableFraction extends Fraction {

    public ExpandableAdapter adapter;

    public ExpandableFraction() {
        // Required empty public constructor
    }

    public static ExpandableFraction newInstance() {

        return new ExpandableFraction();
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_expandable_fragment, container, false);

        ListContainer listContainer = (ListContainer) view.findComponentById(ResourceTable.Id_recyclerview);
        adapter = new ExpandableAdapter(scatter, GenreDataFactory.makeGenres(), listContainer);
        listContainer.setItemProvider(adapter);

        return view;
    }


}
