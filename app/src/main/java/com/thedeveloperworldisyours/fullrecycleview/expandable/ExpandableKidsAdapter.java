/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thedeveloperworldisyours.fullrecycleview.expandable;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.List;

/**
 * Created by javierg on 16/01/2017.
 */

public class ExpandableKidsAdapter extends BaseItemProvider/*ExpandableRecyclerViewAdapter<ParentViewHolder, KidViewHolder>*/ {

    private List<Artist> groups;
    private LayoutScatter scatter;

    public ExpandableKidsAdapter(LayoutScatter scatter, List<Artist> groups) {
        this.scatter = scatter;
        this.groups = groups;
    }

    @Override
    public int getCount() {
        return groups == null || groups.size() == 0 ? 0 : groups.size();
    }

    @Override
    public Object getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Artist artist = (Artist) groups.get(position);
        if (component == null) {
            component = scatter .parse(ResourceTable.Layout_expandable_kid_list_item, componentContainer, false);
            KidViewHolder kidViewHolder = new KidViewHolder(component);
            component.setTag(kidViewHolder);
            kidViewHolder.setArtistName(artist.getName());
        } else {
            ((KidViewHolder) component.getTag()).setArtistName(artist.getName());
        }
        return component;
    }
}
