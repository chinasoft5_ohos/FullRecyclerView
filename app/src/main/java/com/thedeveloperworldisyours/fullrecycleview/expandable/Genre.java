package com.thedeveloperworldisyours.fullrecycleview.expandable;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.util.List;

/**
 * Created by javierg on 16/01/2017.
 */

public class Genre implements Sequenceable {

    private String title;
    private List<Artist> items;
    private int res;
    private boolean expand = false;

    public Genre(String title, List<Artist> items, int res, boolean expand) {
        this.title = title;
        this.items = items;
        this.res = res;
        this.expand = expand;
    }

    public String getTitle() {
        return title;
    }

    public List<Artist> getItems() {
        return items;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    public int getIconResId() {
        return res;
    }

    public static Producer getCreator() {
        return CREATOR;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        return parcel.writeInt(res) && parcel.writeString(title) && parcel.writeSequenceableList(items) && parcel.writeBoolean(expand);
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.title = parcel.readString();
        this.items = parcel.readSequenceableList(Artist.class);
        this.res = parcel.readInt();
        this.expand = parcel.readBoolean();
        return false;
    }

    public static final Sequenceable.Producer CREATOR = new Sequenceable.Producer() {
        public Genre createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            Genre instance = new Genre(in.readString(), in.readSequenceableList(Artist.class), in.readInt(), in.readBoolean());
            instance.unmarshalling(in);
            return instance;
        }
    };

}
