package com.thedeveloperworldisyours.fullrecycleview.expandable;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * Created by javierg on 16/01/2017.
 */

public class ExpandableAdapter extends BaseItemProvider/*ExpandableRecyclerViewAdapter<ParentViewHolder, KidViewHolder>*/ {

    private List<Genre> groups;
    private ListContainer listContainer;
    private LayoutScatter scatter;

    public ExpandableAdapter(LayoutScatter scatter, List<Genre> groups, ListContainer listContainer) {
        this.scatter = scatter;
        this.groups = groups;
        this.listContainer = listContainer;
    }

    @Override
    public int getCount() {
        return groups == null || groups.size() == 0 ? 0 : groups.size();
    }

    @Override
    public Object getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Genre genre = (Genre) groups.get(position);
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_expandable_parent_list_item, componentContainer, false);
            ParentViewHolder dataObjectHolder = new ParentViewHolder(scatter, component, groups, position, listContainer, new ParentViewHolder.ExpandListener() {
                @Override
                public void expand() {
                    notifyDataChanged();
                }
            });
            component.setTag(dataObjectHolder);
            dataObjectHolder.setGenreTitle(genre);
        } else {
            ((ParentViewHolder) component.getTag()).setGenreTitle(genre);
        }
        return component;
    }
}
