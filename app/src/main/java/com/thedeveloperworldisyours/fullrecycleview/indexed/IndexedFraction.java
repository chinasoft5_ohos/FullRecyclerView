package com.thedeveloperworldisyours.fullrecycleview.indexed;

import com.ohos.IndexFastScrollRecyclerView;
import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.sections.model.ElementList;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.*;

public class IndexedFraction extends Fraction {

    HashMap<String, Integer> mMapIndex;
    String[] mSections;
    List<String> fruits;

    private ArrayList<ElementList> dataSet;

    public IndexedFraction() {
        // Required empty public constructor
    }

    public static IndexedFraction newInstance() {
        return new IndexedFraction();
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_indexed_fragment, container, false);

        IndexFastScrollRecyclerView mListContainer = (IndexFastScrollRecyclerView) view.findComponentById(ResourceTable.Id_indexed_fragment_recycler_view);
        mListContainer.enableFadeEffect(Component.FadeEffectEnum.FADEEFFECT_SCROLLBAR);

        dataSet = getDataSet();
        IndexedAdapter mAdapter = new IndexedAdapter(scatter, dataSet, Arrays.asList(mSections), mMapIndex);
        mListContainer.setItemProvider(mAdapter);
        return view;
    }


    private ArrayList<ElementList> getDataSet() {
        String[] fruits = {
                "Apples", "Apricots", "Avocado", "Annona", "Banana",
                "Bilberry", "Blackberry", "Custard", "Clementine", "Cantalope",
                "Coconut", "Currant", "Cherry", "Cherimoya", "Date",
                "Damson", "Durian", "Elderberry", "Fig", "Feijoa",
                "Grapefruit", "Grape", "Gooseberry", "Guava", "Honeydew",
                "Huckleberry", "Jackfruit", "Juniper", "Jambul", "Jujube",
                "Kiwi", "Kumquat", "Lemons", "Limes", "Lychee",
                "Mango", "Mandarin", "Mangostine", "Nectaraine", "Orange",
                "Olive", "Prunes", "Pears", "Plum", "Pineapple",
                "Peach", "Papaya", "Passionfruit", "Pomegranate", "Pomelo",
                "Raspberries", "Rock melon", "Rambutan", "Strawberries", "Sweety",
                "Salmonberry", "Satsuma", "Tangerines", "Tomato", "Ugli",
                "Watermelon", "Woodapple"
        };

        List<String> fruitList = Arrays.asList(fruits);
        getListIndexed(fruitList);

        ArrayList results = new ArrayList<>();
        ElementList obj;
        int section = 0;
        int normal = 0;
        String fruit;
        String ch;
        int total = fruitList.size() + mSections.length;
        for (int index = 0; index < total; index++) {

            fruit = fruitList.get(normal);
            ch = fruit.substring(0, 1);

            if (index == 0 || ch.equals(mSections[section])) {

                obj = new ElementList(ch, true);
                mMapIndex.put(ch, index);
                if (section < mSections.length - 1) {
                    section++;
                } else {
                    section = 0;
                }
            } else {
                obj = new ElementList(fruitList.get(normal), false);
                normal++;
            }

            results.add(index, obj);
        }
        return results;
    }

    public void getListIndexed(List<String> fruitList) {

        this.fruits = fruitList;
        if (mMapIndex == null) {
            mMapIndex = new LinkedHashMap<>();
        }

        for (int x = 0; x < fruits.size(); x++) {
            String fruit = fruits.get(x);
            String ch = fruit.substring(0, 1);
            ch = ch.toUpperCase(Locale.US);

            // HashMap will prevent duplicates
            mMapIndex.put(ch, x);
        }

        Set<String> sectionLetters = mMapIndex.keySet();

        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<>(sectionLetters);

        Collections.sort(sectionList);
        mSections = new String[sectionList.size()];

        sectionList.toArray(mSections);
    }

}
