package com.thedeveloperworldisyours.fullrecycleview.horizontal;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

public class HorizontalFraction extends Fraction {

    public HorizontalFraction() {
        // Required empty public constructor
    }

    public static HorizontalFraction newInstance() {
        HorizontalFraction fragment = new HorizontalFraction();
        return fragment;
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_horizontal_fragment, container, false);
        ListContainer mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_fragment_horizontal_recycler_view);
        HorizontalRecyclerViewAdapter mAdapter = new HorizontalRecyclerViewAdapter(scatter, getDataSet());
        mListContainer.setItemProvider(mAdapter);
        mListContainer.setOrientation(Component.HORIZONTAL);

        // Code to Add an item with default animation
        // ((MyRecyclerViewAdapter) mAdapter).addItem(obj, index);

        // Code to remove an item with default animation
        // ((MyRecyclerViewAdapter) mAdapter).deleteItem(index);

        return view;
    }


    private ArrayList<HorizontalData> getDataSet() {
        ArrayList results = new ArrayList<>();
        for (int index = 0; index < 20; index++) {
            HorizontalData obj = new HorizontalData("Some Primary Text " + index,
                    "Secondary " + index);
            results.add(index, obj);
        }
        return results;
    }
}
