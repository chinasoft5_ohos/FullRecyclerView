package com.thedeveloperworldisyours.fullrecycleview.horizontal;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by javierg on 12/10/2016.
 */

public class HorizontalRecyclerViewAdapter extends BaseItemProvider {
    private ArrayList<HorizontalData> mDataset;
    private LayoutScatter scatter;

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }


    public static class DataObjectHolder {
        Text mLabel;
        Text mDateTime;

        public DataObjectHolder(Component itemView) {
            mLabel = (Text) itemView.findComponentById(ResourceTable.Id_horizontal_list_item_text_view);
            mDateTime = (Text) itemView.findComponentById(ResourceTable.Id_horizontal_list_item_text_view_two);
        }
    }

    public HorizontalRecyclerViewAdapter(LayoutScatter scatter, ArrayList<HorizontalData> myDataset) {
        this.scatter = scatter;
        mDataset = myDataset;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder;
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_horizontal_list_item, componentContainer, false);
            dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }

        dataObjectHolder.mLabel.setText(mDataset.get(position).getmTitle());
        dataObjectHolder.mDateTime.setText(mDataset.get(position).getmSubTitle());
        return component;
    }


    public void addItem(HorizontalData dataObj, int index) {
        mDataset.add(dataObj);
        notifyDataSetItemChanged(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyDataSetItemChanged(index);
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

}
