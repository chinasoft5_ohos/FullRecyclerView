package com.thedeveloperworldisyours.fullrecycleview.single;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.Arrays;

public class SingleFraction extends Fraction implements SingleRecyclerViewAdapter.SingleClickListener {

    SingleRecyclerViewAdapter mAdapter;

    public SingleFraction() {
        // Required empty public constructor
    }

    public static SingleFraction newInstance() {
        return new SingleFraction();
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_single_fragment, container, false);
        ListContainer mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_single_fragment_recycler_view);
        String[] list = new String[]{"Jimi Hendrix", "David Bowie", "Jim Morrison", "Elvis Presley",
                "Mick Jagger", "Kurt Cobain", "Bob Dylan", "John Lennon", "Freddie Mercury", "Elton John", "Eric Clapton"};

        mAdapter = new SingleRecyclerViewAdapter(scatter, Arrays.asList(list));
        mListContainer.setItemProvider(mAdapter);
        mAdapter.setOnItemClickListener(this);
        return view;
    }


    @Override
    public void onItemClickListener(int position, Component view) {
        mAdapter.selectedItem();
    }
}
