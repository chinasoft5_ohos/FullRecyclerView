package com.thedeveloperworldisyours.fullrecycleview.stickyheader;

/**
 * 接口
 *
 * @author javierg
 * @version 1.0, 12/10/2016
 */

public interface ItemData<T> {
    ItemType getItemType();

    String getFirstLetter();

    T getItem();

    enum ItemType {
        TYPE_HEADER(0x99),
        TYPE_CONTENT(0);

        ItemType(int value) {
            this.value = value;
        }

        int value;
    }
}
