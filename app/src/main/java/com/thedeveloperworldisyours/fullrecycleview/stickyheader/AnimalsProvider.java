package com.thedeveloperworldisyours.fullrecycleview.stickyheader;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.List;

public abstract class AnimalsProvider<T> extends BaseItemProvider {

    protected Context context;
    private ListContainer mListContainer;
    protected List<ItemData<T>> list;
    private Component recentHeaderComponent;
    private int lastPosition;

    public AnimalsProvider(Context context, ListContainer listContainer, List<ItemData<T>> items) {
        super();
        list = items;
        this.context = context;
        this.mListContainer = listContainer;
        initBindStateListener();
    }

    private void initBindStateListener() {
        mListContainer.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                addRecentHeaderComponent();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });
        mListContainer.setScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int newY, int i2, int oldY) {
                //找当前第一个可见的item
                int position = mListContainer.getItemPosByVisibleIndex(0);
                int headerHeight = recentHeaderComponent.getEstimatedHeight();

                int nextPosition;
                //找到下一个header的位置
                if (newY > oldY) {
                    //向上拖拽
                    nextPosition = findNextHeaderFromPositionByUp(position);
                    if (nextPosition == -1) {
                        //最后一个
                        recentHeaderComponent.setTranslationY(0);
                        updateFloat(position);
                        return;
                    }
                    Component nextHeaderChild = mListContainer.getComponentAt(nextPosition);
                    if (nextHeaderChild != null) {
                        //说明下一个头已经显示到屏幕范围之内了
                        if (nextHeaderChild.getTop() <= headerHeight) {
                            recentHeaderComponent.setTranslationY(-(headerHeight - nextHeaderChild.getTop()));
                        } else {
                            recentHeaderComponent.setTranslationY(0);
                        }
                        updateFloat(position);
                    } else {
                        recentHeaderComponent.setTranslationY(0);
                    }
                } else {
                    //向下拖拽
                    if (position == list.size() - 1) {
                        updateFloat(position);
                        return;
                    }
                    if (getItemComponentType(position + 1) == ItemData.ItemType.TYPE_HEADER.value) {
                        Component preHeaderChildNext = mListContainer.getComponentAt(position + 1);
                        if (preHeaderChildNext != null) {
                            //说明上一个头的下面的一个child已经显示到屏幕范围之内了
                            if (preHeaderChildNext.getTop() >= 0 && preHeaderChildNext.getTop() <= headerHeight) {
                                recentHeaderComponent.setTranslationY(preHeaderChildNext.getTop() - headerHeight);
                            } else {
                                recentHeaderComponent.setTranslationY(0);
                            }
                            updateFloat(position);
                        } else {
                            recentHeaderComponent.setTranslationY(0);
                        }
                    } else {
                        recentHeaderComponent.setTranslationY(0);
                    }
                }
                if (lastPosition != position) {
                    updateFloat(position);
                }
                lastPosition = position;
            }
        });
    }

    private void updateFloat(int position) {
        updateFloatHeaderComponent(position, recentHeaderComponent);
        //tips：推测是华为的缓冲机制导致，text刷新不及时，只能手动layout了
        recentHeaderComponent.postLayout();
    }

    private void addRecentHeaderComponent() {
        if (recentHeaderComponent == null) {
            return;
        }
        if (recentHeaderComponent.getComponentParent() == null) {
            //这里是要保证悬浮头的位置和列表里面顶部头的位置要一致
            ComponentContainer componentContainer = (ComponentContainer) mListContainer.getComponentParent();
            ComponentContainer.LayoutConfig layoutConfig = mListContainer.getLayoutConfig();

            ComponentContainer.LayoutConfig headerComponentLayoutConfig = recentHeaderComponent.getLayoutConfig();
            layoutConfig.height = headerComponentLayoutConfig.height;
            layoutConfig.width = headerComponentLayoutConfig.width;

            componentContainer.addComponent(recentHeaderComponent, layoutConfig);
        }
    }

    protected abstract void updateFloatHeaderComponent(int position, Component recentHeaderComponent);

    private int findNextHeaderFromPositionByUp(int position) {
        if (position == list.size() - 1) {
            return -1;
        }
        for (int i = position + 1; i < list.size(); i++) {
            if (getItemComponentType(i) == ItemData.ItemType.TYPE_HEADER.value) {
                return i;
            }
        }
        return -1;
    }

    protected int getCurrentFloatTopHeaderPosition() {
        int position = mListContainer.getItemPosByVisibleIndex(0);

        for (int i = position; i >= 0; i--) {
            if (getItemComponentType(i) == ItemData.ItemType.TYPE_HEADER.value) {
                return i;
            }
        }
        return -1;
    }

    protected int getHeaderCountFront(int position) {
        int result = 0;
        for (int i = position - 1; i >= 0; i--) {
            if (getItemComponentType(i) == ItemData.ItemType.TYPE_HEADER.value) {
                result++;
            }
        }
        return result;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public ItemData<T> getItem(int i) {
        return list.get(i);
    }

    @Override
    public int getItemComponentType(int position) {
        return list.get(position).getItemType().value;
    }

    @Override
    final public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        int itemComponentType = getItemComponentType(position);
        Component convert;
        if (itemComponentType == ItemData.ItemType.TYPE_HEADER.value) {
            if (recentHeaderComponent == null) {
                recentHeaderComponent = getHeaderComponent(position, null, componentContainer);
                recentHeaderComponent.setTag(null);
            }
            convert = getHeaderComponent(position, null, componentContainer);
        } else {
            convert = getContentComponent(position, component, componentContainer);
        }

        return convert;
    }

    protected abstract Component getContentComponent(int position, Component component, ComponentContainer componentContainer);


    protected abstract Component getHeaderComponent(int position, Component component, ComponentContainer componentContainer);

}
