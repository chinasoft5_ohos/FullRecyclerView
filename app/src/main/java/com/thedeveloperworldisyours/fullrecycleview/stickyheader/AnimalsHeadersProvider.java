package com.thedeveloperworldisyours.fullrecycleview.stickyheader;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.List;

public class AnimalsHeadersProvider extends AnimalsProvider<String> {

    public AnimalsHeadersProvider(Context context, ListContainer listContainer, List<ItemData<String>> items) {
        super(context, listContainer, items);
        this.context = context;
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getItem().charAt(0);
    }

    @Override
    protected void updateFloatHeaderComponent(int position, Component recentHeaderComponent) {
        if (recentHeaderComponent == null) {
            return;
        }
        //这里加缓存只是为了避免频繁的去findComponentById(虽然此处的recentHeaderComponent就是Text)
        StickyHeaderHolder stickyHeaderHolder;
        if (recentHeaderComponent.getTag() == null) {
            stickyHeaderHolder = new StickyHeaderHolder();
            stickyHeaderHolder.text = ((Text) recentHeaderComponent);//如果不是Text本身，就需要findComponentById
            recentHeaderComponent.setTag(stickyHeaderHolder);
        } else {
            stickyHeaderHolder = (StickyHeaderHolder) recentHeaderComponent.getTag();
        }
        String firstLetter = list.get(position).getFirstLetter();
        stickyHeaderHolder.text.setText(firstLetter);
        stickyHeaderHolder.text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                int topHeaderPosition = getCurrentFloatTopHeaderPosition();
                showToast("Header position:" + (topHeaderPosition - getHeaderCountFront(topHeaderPosition)) + " id:" + getItemId(topHeaderPosition));
            }
        });
    }

    @Override
    protected ohos.agp.components.Component getContentComponent(int position, Component component, ComponentContainer
            componentContainer) {
        StickyContentHolder contentHolder;
        ItemData<String> itemData = list.get(position);
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_content, null, false);
            component.setLayoutConfig(new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));
            Text text = (Text) component.findComponentById(ResourceTable.Id_text);
            contentHolder = new StickyContentHolder();
            contentHolder.text = text;
            contentHolder.letter = (Text) component.findComponentById(ResourceTable.Id_tv_indicator);
            component.setTag(contentHolder);
        } else {
            contentHolder = (StickyContentHolder) component.getTag();
        }
        contentHolder.text.setText(itemData.getItem());
        contentHolder.letter.setText(itemData.getFirstLetter().toUpperCase());
        return component;
    }

    @Override
    protected Component getHeaderComponent(int position, Component component, ComponentContainer componentContainer) {
        StickyHolder headerHolder;
        ItemData<String> itemData = list.get(position);
        if (component == null) {
            Text text = new Text(context);
            text.setTextSize(84);
            text.setTextColor(Color.BLACK);
            //粗细
            text.setFont(new Font.Builder("animals").setWeight(Font.BOLD).build());
            text.setTextAlignment(TextAlignment.CENTER);
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
            text.setBackground(shapeElement);
            text.setLayoutConfig(new StackLayout.LayoutConfig(120, 120));
            component = text;
            headerHolder = new StickyHolder();
            headerHolder.text = text;
            component.setTag(headerHolder);
        } else {
            headerHolder = (StickyHolder) component.getTag();
        }

        headerHolder.text.setText(itemData.getFirstLetter().toUpperCase());
        headerHolder.text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showToast("Header position:" + (position - getHeaderCountFront(position)) + " id:" + getItemId(position));
            }
        });
        return component;
    }

    static class StickyHolder {
        Text text;
    }

    static class StickyContentHolder {
        Text text;
        Text letter;
    }

    static class StickyHeaderHolder {
        Text text;
    }


    private void showToast(String msg) {
        ToastDialog toastDialog = new ToastDialog(context);

        ShapeElement shapeElement = new ShapeElement();
        RgbColor rgbColor = RgbColor.fromArgbInt(Color.getIntColor("#aadddddd"));
        shapeElement.setRgbColor(rgbColor);
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setCornerRadius(60);
        Text text = new Text(context);
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                120);
        text.setLayoutConfig(layoutConfig);
        text.setBackground(shapeElement);
        text.setMultipleLine(true);
        text.setTextSize(36);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setTextColor(Color.BLACK);
        text.setText(msg);
        text.setPadding(60, 30, 60, 30);
        toastDialog.setTransparent(true);
        toastDialog.setComponent(text);
        toastDialog.setOffset(0, DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().height / 2 - 120);
        toastDialog.setAlignment(5);
        toastDialog.show();
    }
}
