package com.thedeveloperworldisyours.fullrecycleview.updateData;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * Created by javierg on 30/10/2017.
 */

public class UpdateDataFraction extends Fraction implements ListContainer.ItemClickedListener {

    ListContainer mListContainer;

    private UpdateDataAdapter mAdapter;

    private static final int MULTIPLE = 0;

    public UpdateDataFraction() {
        // Required empty public constructor
    }

    public static UpdateDataFraction newInstance() {
        return new UpdateDataFraction();
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_update_data_fragment, container, false);
        mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_update_fragment_recycler_view);

        mAdapter = new UpdateDataAdapter(scatter, getDataSet(), MULTIPLE);
        mListContainer.setItemProvider(mAdapter);
        mListContainer.setItemClickedListener(this);
        return view;
    }


    public static ArrayList<UpdateData> getDataSet() {
        ArrayList results = new ArrayList<>();
        for (int index = 0; index < 20; index++) {
            UpdateData obj = new UpdateData("Some Primary Text " + index,
                    "Secondary " + index, false);
            results.add(index, obj);
        }
        return results;
    }

    public void changeMode(int modo) {
        mAdapter.changeMode(modo);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
        mAdapter.selected(position);
    }
}
