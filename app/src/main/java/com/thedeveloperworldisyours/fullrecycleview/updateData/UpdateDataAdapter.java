package com.thedeveloperworldisyours.fullrecycleview.updateData;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by javierg on 30/10/2017.
 */

public class UpdateDataAdapter extends BaseItemProvider {

    private static final int MULTIPLE = 0;
    private static final int SINGLE = 1;
    private int sModo = 0;
    private ArrayList<UpdateData> mDataSet;
    private LayoutScatter scatter;

    static class DataObjectHolder {
        Text mLabel;
        Text mDateTime;
        DirectionalLayout mBackground;

        DataObjectHolder(Component itemView) {
            mLabel = (Text) itemView.findComponentById(ResourceTable.Id_vertical_list_item_title);
            mDateTime = (Text) itemView.findComponentById(ResourceTable.Id_vertical_list_item_subtitle);
            mBackground = (DirectionalLayout) itemView.findComponentById(ResourceTable.Id_vertical_list_item_background);
        }
    }

    UpdateDataAdapter(LayoutScatter scatter,ArrayList<UpdateData> myDataset, int modo) {
        this.scatter = scatter;
        this.mDataSet = myDataset;
        this.sModo = modo;
    }

    @Override
    public int getCount() {
        return mDataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder;
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_update_list_item, componentContainer, false);
            dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }
        UpdateData updateData = mDataSet.get(position);
        dataObjectHolder.mLabel.setText(updateData.getmTitle());
        dataObjectHolder.mLabel.setTextColor(updateData.getSelected() ?
                new Color(componentContainer.getContext().getColor(ResourceTable.Color_colorAccent)) : Color.BLACK);
        dataObjectHolder.mDateTime.setText(updateData.getmSubTitle());
        dataObjectHolder.mBackground.setBackground(getBackgroundElement(updateData.getSelected() ?
                new RgbColor(168, 168, 168) : new RgbColor(0, 211, 255)));
        return component;
    }

    private Element getBackgroundElement(RgbColor color) {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(color);
        return element;
    }


    public void selected(int position) {
        switch (sModo) {
            case SINGLE:
                int tPosition = getSelectedPosition();
                if (tPosition != position) {
                    for (UpdateData updateData : mDataSet) {
                        updateData.setSelected(false);
                    }
                }
                break;
            case MULTIPLE:
                break;
            default:
                break;
        }
        mDataSet.get(position).setSelected(!mDataSet.get(position).getSelected());
        notifyDataChanged();
    }

    private int getSelectedPosition() {
        int sPos = -1;
        for (int i = 0; i < mDataSet.size(); i++) {
            Boolean selected = mDataSet.get(i).getSelected();
            if (selected) {
                sPos = i;
                break;
            }
        }
        return sPos;
    }

    public void changeMode(int modo) {
        sModo = modo;
        for (UpdateData updateData : mDataSet) {
            updateData.setSelected(false);
        }
        notifyDataChanged();
    }

}
