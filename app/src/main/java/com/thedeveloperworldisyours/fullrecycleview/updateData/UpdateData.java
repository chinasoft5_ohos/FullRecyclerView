package com.thedeveloperworldisyours.fullrecycleview.updateData;

/**
 * 实体类
 *
 * @author javierg
 * @version 1.0, 25/01/2017
 */

public class UpdateData {
    private String mTitle;
    private String mSubTitle;
    private Boolean selected;

    UpdateData(String title, String subTitle,Boolean selecte){
        mTitle = title;
        mSubTitle = subTitle;
        selected = selecte;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmSubTitle() {
        return mSubTitle;
    }

    public void setmSubTitle(String mSubTitle) {
        this.mSubTitle = mSubTitle;
    }
}