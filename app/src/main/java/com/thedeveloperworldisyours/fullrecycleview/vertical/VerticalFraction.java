package com.thedeveloperworldisyours.fullrecycleview.vertical;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import com.thedeveloperworldisyours.fullrecycleview.common.CustomDialog;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

public class VerticalFraction extends Fraction implements ListContainer.ItemClickedListener {

    private ListContainer mListContainer;
    private VerticalRecyclerViewAdapter mAdapter;
    private Context context;

    public VerticalFraction() {
        // Required empty public constructor
    }

    public static VerticalFraction newInstance() {
        return new VerticalFraction();
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // Inflate the layout for this fragment
        Component view = scatter.parse(ResourceTable.Layout_vertical_fragment, container, false);

        context = container.getContext();
        mListContainer = (ListContainer) view.findComponentById(ResourceTable.Id_vertical_fragment_recycler_view);
        mListContainer.setScrollbarFadingEnabled(true);

        mAdapter = new VerticalRecyclerViewAdapter(scatter, getDataSet());
        mListContainer.setItemProvider(mAdapter);
        mListContainer.setItemClickedListener(this);
        return view;
    }


    private ArrayList<VerticalData> getDataSet() {
        ArrayList results = new ArrayList<>();
        for (int index = 0; index < 20; index++) {
            VerticalData obj = new VerticalData("Some Primary Text " + index,
                    "Secondary " + index);
            results.add(index, obj);
        }
        return results;
    }

    public void addItem() {
        Component parse = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_vertical_dialog, null, false);
        ((Text) parse.findComponentById(ResourceTable.Id_dialog_title)).setText(context.getString(ResourceTable.String_vertical_fragment_title_dialog_add));
        ((Text) parse.findComponentById(ResourceTable.Id_dialog_content)).setText(context.getString(ResourceTable.String_vertical_fragment_question_add));
        CustomDialog dialog = new CustomDialog(context, parse);
        Button yes = (Button) parse.findComponentById(ResourceTable.Id_dialog_yes);
        Button no = (Button) parse.findComponentById(ResourceTable.Id_dialog_no);
        yes.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // continue with delete
                actionAdd();
                dialog.hide();
            }
        });

        no.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // do nothing
                dialog.hide();
            }
        });

        dialog.setAutoClosable(true);
        dialog.show();
    }

    public void actionAdd() {
        VerticalData object = new VerticalData("Some Primary Text " + mAdapter.getCount(),
                "Secondary " + mAdapter.getCount());
        mAdapter.addItem(object, mAdapter.getCount());
        mListContainer.scrollTo(mAdapter.getCount() - 1);

    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
        Component parse = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_vertical_dialog, null, false);
        ((Text) parse.findComponentById(ResourceTable.Id_dialog_title)).setText(context.getString(ResourceTable.String_vertical_fragment_title_dialog_delete));
        ((Text) parse.findComponentById(ResourceTable.Id_dialog_content)).setText(context.getString(ResourceTable.String_vertical_fragment_question_delete));
        CustomDialog dialog = new CustomDialog(context, parse);
        Button yes = (Button) parse.findComponentById(ResourceTable.Id_dialog_yes);
        Button no = (Button) parse.findComponentById(ResourceTable.Id_dialog_no);
        yes.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // continue with delete
                mAdapter.deleteItem(position, listContainer);
                dialog.hide();
            }
        });

        no.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // do nothing
                dialog.hide();
            }
        });

        dialog.setAutoClosable(true);
        dialog.show();
    }
}
