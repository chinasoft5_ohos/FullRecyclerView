package com.thedeveloperworldisyours.fullrecycleview.vertical;

import com.thedeveloperworldisyours.fullrecycleview.ResourceTable;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by javierg on 25/01/2017.
 */

public class VerticalRecyclerViewAdapter extends BaseItemProvider {

    private ArrayList<VerticalData> mDataset;
    private LayoutScatter scatter;

    static class DataObjectHolder implements Component.ClickedListener {
        Text mLabel;
        Text mDateTime;

        DataObjectHolder(Component itemView) {
            mLabel = (Text) itemView.findComponentById(ResourceTable.Id_vertical_list_item_title);
            mDateTime = (Text) itemView.findComponentById(ResourceTable.Id_vertical_list_item_subtitle);
            itemView.setClickedListener(this);
        }

        @Override
        public void onClick(Component component) {

        }
    }


    VerticalRecyclerViewAdapter(LayoutScatter scatter, ArrayList<VerticalData> myDataset) {
        this.scatter = scatter;
        this.mDataset = myDataset;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        DataObjectHolder dataObjectHolder;
        if (component == null) {
            component = scatter.parse(ResourceTable.Layout_vertical_list_item, componentContainer, false);
            dataObjectHolder = new DataObjectHolder(component);
            component.setTag(dataObjectHolder);
        } else {
            dataObjectHolder = (DataObjectHolder) component.getTag();
        }
        dataObjectHolder.mLabel.setText(mDataset.get(position).getmTitle());
        dataObjectHolder.mDateTime.setText(mDataset.get(position).getmSubTitle());
        return component;
    }

    void addItem(VerticalData dataObj, int index) {
        mDataset.add(dataObj);
        notifyDataSetItemRangeInserted(index, 1);
    }

    void deleteItem(int index, ListContainer listContainer) {
        if (index <= mDataset.size() - 1) {
            Component child = listContainer.getComponentAt(index);
            int height = child.getHeight();
            AnimatorValue animatorValue = new AnimatorValue();
            animatorValue.setDuration(300);
            animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float value) {
                    float tempHeight = (height * (Float.sum(-value, 1f)));
                    if (tempHeight == 0) {
                        mDataset.remove(index);
                        notifyDataSetItemRemoved(index);
                        child.setHeight(height);
                        notifyDataChanged();
                    } else {
                        child.setHeight((int) tempHeight);
                        notifyDataSetItemChanged(index);
                    }
                }
            });
            animatorValue.start();

        }
    }

}
